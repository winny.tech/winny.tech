+++
title = "Contact"
author = ["Winston Weinert (winny)"]
date = 2023-11-20T16:19:00-06:00
draft = false
+++

Email
: [hello@winny.tech](mailto:hello@winny.tech)

LinkedIn
: [winstonweinert](https://www.linkedin.com/in/winstonweinert/)

Signal
: `winny.97` ([Click here](https://signal.me/#eu/qd2gMv8o3UQuJ87xIcud_S1WjKOQFsWneh7f-DWMUnLWTy5OEvVDMKmOJ-F5lFN0) to start a chat.)


## PGP Key {#pgp-key}

My PGP key fingerprint is `02C6 59B8 1677 0510 27AF D836 F84E DB1C 40ED BE3A`.

Here's a copy of my key: [02C659B81677051027AFD836F84EDB1C40EDBE3A.asc](/02C659B81677051027AFD836F84EDB1C40EDBE3A.asc)
