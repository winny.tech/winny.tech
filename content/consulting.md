+++
title = "Consulting"
author = ["Winston Weinert (winny)"]
date = 2024-08-28T16:34:00-05:00
draft = false
+++

Are you an individual or small business looking for a techie that can build you
a website or manage your servers?  I'm your guy.  Write me at
[consulting@winny.tech](mailto:consulting@winny.tech) and let's accomplish the impossible together!


## Portfolio {#portfolio}

Here are a collection of websites and services that I have built.  You might
also like to read my [resume]({{< relref "resume" >}}).

-   Wordpress powered website - [PEO Chapter J — Manitowoc, WI](https://chapterjmanitowoc.wordpress.com/)
-   Previous admin of NordicResults infrastructure - private GitLab, Redmine,
    Dedicated servers, backups, disaster recovery, Ansible, OpenVZ, Docker.
-   Lots of time working with older and younger generations on computer literacy.
-   I also [tutor]({{< relref "tutoring" >}}) software programming skills!
