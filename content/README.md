+++
title = "README"
author = ["Winston Weinert (winny)"]
date = 2019-11-20T19:06:00-06:00
draft = false
+++

## ox-hugo powered homepage {#ox-hugo-powered-homepage}

The code is located on [GitLab official hosting](https://gitlab.com/winny.tech/winny.tech). This website and
project files are licensed under [CC-BY-SA-4.0](../LICENSE.txt).


## Signatures {#signatures}


### Creating {#creating}

```bash
gpg --detach --armor --sign YOURFILE
```


### Verifying {#verifying}

```bash
gpg --verify YOURFILE
```
