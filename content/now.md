+++
title = "Now"
author = ["Winston Weinert (winny)"]
date = 2023-08-03T17:39:00-05:00
draft = false
+++

Inspired by [nownownow.com](https://nownownow.com/) and Derek Sivers' [post on the matter](https://sive.rs/nowff).  This webpage
describes some of the things that I am currently up to.  A sort of public
declaration of my TODO.


## Looking for a better work experience {#looking-for-a-better-work-experience}

I found that I detest 9-to-5 work schedules, corporate in-authenticity (well I
can manage this one), and "ass-in-seat" work.  Most "ass-in-seat" work is
thinly veiled busywork which provides little to no value to yourself, and
slightly more value to the org.

I've decided I don't enjoy tech as an industry --- I am an artist and want to do
my best work.  Tech isn't about doing your best work --- it's about results or
research.  Both are frustrating for the artist.  Still, I'm open to devops and
webdev work.

I recently launched a consultant brand [shipit.consulting](https://shipit.consulting/) to funnel work to a
single website.  No bites yet, so need to improve the website and come up with a
marketing plan.


## Growth mindset {#growth-mindset}

Had a spiritual awakening.  Realized I was spending my time in ways that
weren't going to yield an emotional or financial return on investment.

Here's the changes that I've made since:

1.  ****Attending the gym.**** it turned out most of my physical health issues were
    caused by inactivity.  Physical therapy was a scam to keep me weak and I'm
    doing much better being in charge of my own fitness.  (Listen to your body,
    a PE therapist may still help you - I'm just a voice on the internet!)

2.  ****Stretching most days.****  Hopefully I learn Yoga to keep this interesting.
    I can squat without pain now.  Isn't that something?

3.  ****Meal prepping.****  I have many food issues.  In short I can't or won't eat
    most food served to me.  Meal prepping has reduced my time in the kitchen by
    at least 30 minutes to an hour daily.  _Make food from scratch --- there
    is no shortcut._

4.  ****In-person networking.**** Gave it a go.  I see potential.  Need to
    differentiate myself and rebrand myself, then try again.  [Digital business
    cards](/card) are messed up --- nowhere near as smooth as a physical card.  I have
    real cards, ask for one next time you see me.

5.  ****Giving myself space to grow creative skills.**** I am my own worst critic,
    however, being told incessantly as a child that you have no creative skill
    embodies this vibe of helplessness; turns out all there is needed to fix it
    is simply reject these childhood memories and try again.  Pleased to say my
    writing skills are rapidly improving as are sketching and design skills.

6.  ****Reduced chats.****  I joined MSN as a kid, graduated to XMPP and IRC.
    Finally settled on just Discord, Matrix, and Signal.  Still two too many
    services --- eeek.  IRC was no longer serving as a community building
    exercise and is full of people complaining that nobody comes on IRC anymore,
    instead of changing with their environment.


## Working on rebranding {#working-on-rebranding}

In an effort to increase deliberateness in my life, I've begun a rebrand.  We
all have a brand --- how others see us.  Turns out it can be important for
gaining mindshare, hiring people, or to maintain a certain hire-able status.
