+++
title = "Donate"
author = ["Winston Weinert (winny)"]
date = 2022-06-06T19:07:00-05:00
draft = false
+++

I spend a good amount of time contributing to open source projects and
community efforts.  In particular I enjoy mentoring and teaching.  This isn't
for free, it costs time.  If you like what I do, please consider sharing the
wealth.


## Donate to me directly {#donate-to-me-directly}

-   [Patreon](https://www.patreon.com/winnytech?fan_landing=true) - Good for recurring donations and perks
-   [Buy me a Coffee](https://www.buymeacoffee.com/winny) - Good for once-off donations


## Affiliate and referral links {#affiliate-and-referral-links}

-   [Vultr server hosting sign up - get $100 free credit](https://www.vultr.com/?ref=7291870)
-   [Digital Ocean server hosting sign up - get $100 free credit](https://m.do.co/c/df1c36a85706)
-   [Ting Mobile cellphone provider - get $25 free credit.](https://legacy.ting.com/r/zg9i8adgbi91)  I pay $27/mo for my
    cellphone and that's more than enough data for my pedestrian needs.


## Other winny-approved causes {#other-winny-approved-causes}

-   [Institute of Justice](https://ij.org/)
-   [Free Software Conservancy](https://sfconservancy.org/donate/)
-   [Electronic Frontier Foundation](https://supporters.eff.org/donate/join-eff-m--h)

_Thank you, for visiting this page 🙏._
