+++
title = "Tutoring"
author = ["Winston Weinert (winny)"]
date = 2023-12-17T20:17:00-06:00
draft = false
cover = "student.png"
+++

I offer tutoring and mentoring in Computer Science, learning to code, and
developing a hacker's mindset.

-   Docker, Linux, Containerization
-   Firewalls with tools like pf/iptables/nftables/firewalld
-   Full stack dev
-   Java and JVM ecosystem languages
-   Python
-   NodeJS
-   C and C++ and x86, MIPS Assembly
-   Haskell
-   Lisp

[Contact me]({{< relref "contact" >}}) or start a conversation on [Superprof](https://www.superprof.com/graduate-from-uwm-milwaukee-computer-science-with-years-industry-experience-teaches-python-javascript-and-other-programming.html).
