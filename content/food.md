+++
title = "Food"
author = ["Winston Weinert (winny)"]
date = 2023-11-21T20:54:00-06:00
draft = false
+++

Food is an essential component of human culture.  All cultures have food
traditions.  Unfortunate for me, ****I have intolerance/sensitivy to many
different foods****.  As a silver lining, I have become a intermediate cook.  I
also don't mind cooking and bringing my own food.  I will bring extra servings
upon request - just ask!

This page is designed to help my friends and family understand how to prepare
food for me, or convince them to just let me bring my own food or fast.  I
don't mind fasting, I can eat before and after, and fasting is quite good for the
body anyways.

Thanks again for reading this page; ****I wish I could eat all food****.  I am
sorry for any inconvenience that I may cause by having dietary restrictions.

If you know how to train one's body to tolerate these foods (again), please
[contact me]({{< relref "contact" >}}).


## What can't I eat? {#what-can-t-i-eat}

-   I am ****extremely sensitive to gluten****, no wheat based pasta, bread,
    processed foods.  Consuming gluten will cause symptoms lasting days to weeks,
    even months, so please take care.
-   I don't eat ****oats**** because most oat product is cultivated with a protein which the
    body parses gluten.
-   ****Nightshades**** seem to cause a mild inflammatory response in my body, so ****no
    tomatos, potatoes, peppers**** (black pepper is OK), and [others](https://unboundwellness.com/nightshades/).
-   ****Seed Oils**** cause a mild inflammatory response in my body.  No
    grapeseed oil, certain palm oils, canola, soybean oil.
-   My body can't digest ****dairy**** so it is also off limits.
-   I don't eat ****eggs****.  They seem to cause stomach upset and a runny nose.  Egg
    whites might be okay, ask in advance.
-   I ****avoid processed foods****, because it causes an inflammatory response in my
    body which in turns yields a foul mood lasting for days.


## Substitution ideas {#substitution-ideas}

| Bad ingredient                   | Substitute                           |
|----------------------------------|--------------------------------------|
| High heat oil such as canola oil | avocado oil has smoke point 500F     |
|                                  | (or cook with animal fats)           |
| Potato wedges                    | Sweet potato wedges                  |
| Pizza crust                      | Cassava/Tapioca based pizza crust    |
| tomato pizza sauce               | Olive oil base                       |
| cheese flavoring                 | Nutritional yeast                    |
|                                  | (or a potato-free cheese substitute) |
| bread                            | rice bread                           |
| Wheat flour                      | Cassava Flour, Corn Masa             |
