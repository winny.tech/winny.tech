+++
title = "Resume"
author = ["Winston Weinert (winny)"]
date = 2020-05-05T19:09:00-05:00
draft = false
+++

| Email:    | [`hello@winny.tech`](mailto:hello@winny.tech)             |
|-----------|-----------------------------------------------------------|
| Homepage: | [`https://winny.tech/`](https://winny.tech/)              |
| GitHub:   | [`https://github.com/winny-`](https://github.com/winny-/) |


## Experience {#experience}

**Tutoring Services**, Freelance; Milwaukee, WI --- November
2023--Current. Inspire, teach and mentor students learning to code in
JavaScript, Python, Java, Jet-brains, Docker, web development and
technology literacy. Produce marketing copy and ad-rolls.

**Resales**, Freelance; Milwaukee, WI --- February 2023--September 2023.
Refurbish computers and other consumer electronics to sell on eBay.

**Devops Engineer Consultant**, Nordic Results.; Remote --- May
2022--July 2023. Manage infrastructure using Terraform, Ansible in
conjunction with shell scripts, Node.JS, Perl and Python scripts.
Support deployments (via Fab-file), alerting (via Grafana), security
auditing. Built GitLab Continuous Integration (CI) workflows. Rewrote V1
Node.JS backend in NestJS (Typescript). Feature development for Ruby on
Rails backends. Manage server infrastructure, production services for
internal and external customers --- GitLab, Redmine, libvirtd, Ruby on
Rails, PostgreSQL, nginx, Docker, containerization, docker-compose,
Ubuntu servers. Manage dedicated servers with Hetzner and AWS cloud
infrastructure using Terraform.

**Software Engineer Consultant**, Bluebot.; Remote --- September
2022--December 2022. Supported GitHub Continuous Integration (CI)
workflows. Rewrite proof-of-concept NestJS (Typescript with Node.JS,
PostgreSQL, Prisma, Docker) API service for monitoring water flow rate
sensors.

**Software Engineer / Site Reliability Engineer**, Kohl's, Inc.;
Milwaukee, WI --- June 2021--April 2022. Collaborated across multiple
teams ensuring software reliability while adding new features with
DevOps methodologies. Built GitLab Continuous Integration (CI) workflows
for GitOps and deployments. Managed disaster recovery and monitored
services on Oracle Cloud Infrastructure, internal Kubernetes (OpenShift)
and Google Cloud Platform using Splunk and Dynatrace. Supported internal
financial users and develop features for Business Intelligence (BI)
using Qlik, Tableu, Oracle EPM integrations. Authored automations for
change management in Service Now. Modernized Java Spring Boot
applications. Modernized MySQL Stored Procedure code used in ETL data
transfer. Import InfluxDB into MySQL via Bash automation. Migrated OCI
Red Hat hosts into Ansible powered configuration management with custom
Python plugins. Automated GitLab project life cycle tasks. Automated
testing of Oracle EPM updates via headless browser tests using NodeJS
with Cypress.

**Software Developer**, Roydan Enterprises LLC.; Manitowoc, WI --- Sept
2020--June 2021. Help maintain compliance with PCI and SOC2 for a debt
collection SaaS with ConnectWise project management. Modernize OpenEdge
ABL database backend with automated testing and schema migrations using
shell scripts and OpenEdge ABL. Windows C#. NET WinForms application
feature development. Modernized a code-gen tool which generated RPC
calls from OpenEdge ABL source code for C#. Administrated Linux and
Windows hosts. Authored code reviews on GitHub and perform software life
cycle best practices. Modernized TeamCity Continuous Integration (CI)
deployments. Assess Kubernetes, RabbitMQ adoption.

**Research Assistant**, University of Wisconsin-Milwaukee; Milwaukee, WI
--- 2017. Deployed software on CentOS &amp; RHEL using Pkgsrc packages.
Maintained and update packages for Pkgsrc and FreeBSD Ports using Git
and Subversion. Automated workflows using shell scripts and Perl.
Assisted users of High Performance Computing cluster.

**Computer Science Undergrad Grader**, University of Wisconsin-Milwaukee;
Milwaukee, WI --- 2017. Graded Python programming assignments. Graded
Introduction to Computer Science exams. Proctored exams.

**Backend Engineer**, Warthog Financials.; Milwaukee, WI --- 2016--2017.
Modernize backend with JavaScript/Node.JS best practices. Managed
MongoDB and AWS EC2 Linux infrastructure and operations. Agile Sprint
planning and execution with Bitbucket Git projects.


## Education {#education}

University of Wisconsin-Milwaukee, Milwaukee, WI --- B.S. Computer
Science, 2016--Graduated Spring 2020.


## More Skills _(In addition to items in work history.)_ {#more-skills-in-addition-to-items-in-work-history.}

Additional software development in C, C++, PHP, x86 Assembly, MIPS
Assembly, Scala, Django, Golang, Kafka

Experience with Software Project Management as lead developer and
project manager

Packaging software for NixOS, Gentoo, Arch Linux, Alpine Linux, Debian
and Mac Homebrew

Reverse engineering network protocols based on Protobuf, SSL/TLS,
TCP/UDP via Wireshark, Nmap and tcpdump

Technical writing with Markdown, reStructuredText, Textile, MediaWiki,
Man pages, Texinfo and LaTeX


## Activities {#activities}

**Board Member**, McKinley Academy; Manitowoc, WI --- Spring 2021--Spring
2022

**President of IEEE-CS @ UWM**, University of Wisconsin-Milwaukee;
Milwaukee, WI --- Fall 2019--Spring 2020

**Officer of IEEE-CS @ UWM**, University of Wisconsin-Milwaukee;
Milwaukee, WI --- Fall 2017--Spring 2019

**Active in the open source community** --- contributions to VLC, FreeBSD,
PEP8, Racket and more
