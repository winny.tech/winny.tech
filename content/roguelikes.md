+++
title = "Roguelikes"
author = ["Winston Weinert (winny)"]
date = 2022-06-21T19:09:00-05:00
draft = false
cover = "nh.jpg"
+++

Turn based roguelikes are my favorite video game genre.  Nothing beats
investing hours of time in a game only to step on a landmine and die.  No
really, it's quite fun - the losses are as great as the gains!


## Nethack {#nethack}

{{< figure src="/ox-hugo/nh.jpg" >}}

Nethack is my favorite roguelike.  Back in the early oughts, I had an old HP
Compaq PC.  I didn't have a hard drive, so I booted Damn Small Linux on it.
Every time I booted it, I'd install NetHack then play the tiles version.  Back
then I was so new to it I could barely make it to the mines.

Fast forward ten years or so, I was playing on [hardfought.org](https://hardfought.org/) and
[nethack.alt.org](https://nethack.alt.org/).  You can play nethack via `ssh nethack@hardfought.org`.

Here is a table of my publicly viewable ascensions:

| Date    | Role-Race-Sex-Alignment | Dumplog                                                                               | Comment                                                                       |
|---------|-------------------------|---------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| 2017-11 | Val-Dwa-Fem-Law         | [link](https://www.hardfought.org/userdata/w/winny/dn36/dumplog/1510080922.dn36.txt)  | Very first ascension                                                          |
| 2017-12 | Bar-Orc-Mal-Cha         | NAO lost it                                                                           | Lots of mistakes, twice polymorphed into a Silver Dragon (while wearing SDSM) |
| 2018-06 | Val-Dwa-Fem-Law         |                                                                                       |                                                                               |
| 2020-08 | Val-Dwa-Mal-Law         | [link](https://www.hardfought.org/userdata/w/winny/nethack/dumplog/1575962978.nh.txt) | Got polymorphed into a male dwarf at some point.                              |
| 2021-08 | Sam-Hum-Fem-Law         | [link](https://www.hardfought.org/userdata/w/winny/nethack/dumplog/1610987600.nh.txt) | Third role ascended                                                           |

I keep some additional notes on [User:Winny](https://nethackwiki.com/wiki/User:Winny) on nethackwiki.org.


## ZAPM {#zapm}

{{< figure src="/ox-hugo/zapm.jpg" >}}

This is another fun one.  Very quirky humor.  A lot less balanced than NetHack,
but fun none-the-less.  You can play ZAPM via `ssh nethack@hardfought.org`.

Here are my ZAPM wins.  Unfortunately I don't have dumplogs available at this
time:

2017-11
: `winny the Captain Activated the Bizarro Orgasmatron in the
           Mainframe at depth 16. [42301 points]`

2018-05
:



Others
: I think there's some additional wins I failed to record.  It's a
    fairly easy game.


## TSL {#tsl}

[TSL a.k.a. The Slimy Lichmummy](http://www.happyponyland.net/the-slimy-lichmummy) is another fun sci-fi-ish roguelike.  I've won a
few games.  In the scale of difficulty, I'd say it is easier than both ZPAM and
NetHack.


## Jupiter Hell {#jupiter-hell}

{{< figure src="/ox-hugo/jh-survivor.jpg" >}}

Been playing this a lot recently because it has super controller support.  It
is relaxing to play on the couch.  I'd rate this game as more difficult than
TSL and ZAPM (at higher difficulties), but far easier than NetHack.


## Pixel Dungeon {#pixel-dungeon}

{{< figure src="/ox-hugo/pixel-dungeon.jpg" >}}

Somehow I've yet to ascend this game.  I've gotten to the final boss fight
once, long time ago.  I believe this game is harder than NetHack, because you
depend on RNG far more than all the other games in this list.  Pixel Dungeon is
free on Android, a few bucks on iOS.  You can also play on desktop.
