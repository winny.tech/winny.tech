+++
title = "Welcome to winny.tech - Homepage of Winston Weinert"
author = ["Winston Weinert (winny)"]
date = 2023-11-20T17:12:00-06:00
draft = false
+++

Howdy!  My name is Winston Weinert --- some call me winny.  I enjoy day
dreaming, making a difference in peoples' lives, and reading books.

I'm actively searching for opportunities --- see my [resume]({{< relref "resume" >}}) and [works page]({{< relref "works" >}}).
