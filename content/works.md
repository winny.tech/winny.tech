+++
title = "Works"
author = ["Winston Weinert (winny)"]
date = 2023-11-20T16:39:00-06:00
draft = false
+++

See [my resume]({{< relref "resume" >}}).

-   [Writings](#writings)
-   [My technical projects](#my-technical-projects)
-   [Open source contributions](#open-source-contributions)

    ---


## Writings {#writings}


### Blog {#blog}

See my blog at [`blog.winny.tech`](https://blog.winny.tech/).


### Writing a Worm {#writing-a-worm}

During my computer science undergrad at University of Wisconsin Milwaukee, I
had the opportunity to participate in a cybersecurity lab.

We were tasked with a project towards the end of the class and I authored a
simple computer worm.  Much like [panchan](https://www.linode.com/blog/security/linode-security-digest-panchan-malware-lelastic-vulnerability/), it works by using SSH for lateral
movement across hosts.  Unlike panchan, it was never released into the wild and
is not super robust.

-   [Slide deck](</ox-hugo/Winston Weinert - Presentation - Writing a Worm.pdf>)
-   [Final report](</ox-hugo/Winston Weinert - Report - Writing a Worm.pdf>)


## My technical projects {#my-technical-projects}

I sling code on [GitHub](https://github.com/winny-) and [GitLab](https://gitlab.com/winny), occasionally using other
services.


### Active {#active}

Stuff that has been updated in the last year or so goes here.

[Super Rogue](https://github.com/bismuthsoft/super_rogue)
: Spring Lisp Game Jam 2023 submission.  Play [here](https://super-rogue.workinprogress.top/).

[sillypaste](https://github.com/winny/sillypaste)
: A Django (Python) based pastebin with REST API and user
    accounts.  [See this example instance on Heroku.](https://sillypaste.herokuapp.com/)

[jhmod](https://github.com/sector-f/jhmod)
: A tool to manipulate Jupiter Hell save files, .nvc archives (game
    data lives within), and more.  Golang.

[tinybasic.rkt](https://github.com/winny-/tinybasic.rkt)
: TinyBASIC Interpreter in Racket.  [Blog post](https://blog.winny.tech/posts/lang-tinybasic/).

[toml-racket](https://github.com/winny-/toml-racket)
: TOML parser for Racket.  Originally forked from [Greg
    Hendershott's](https://github.com/greghendershott/toml) TOML parser.

[umask](https://github.com/winny-/umask)
: micro library to access umask syscall within Racket.

[4chdl](https://github.com/winny-/4chdl)
: 4chan image downloader in Racket

[aoc](https://github.com/winny-/aoc)
: Advent of code samples in various languages.  [See the year I tried
    to do each day in a different language](https://github.com/winny-/aoc/tree/master/2020#readme).  Favorite days were done in [ZKL](https://github.com/winny-/aoc/blob/master/2020/22/day22.zkl) and
    [Forth](https://github.com/winny-/aoc/blob/master/2020/12/day12.fs).


### Application scripting / configuration {#application-scripting-configuration}

[awesome-cfg](https://gitlab.com/winny/awesome-cfg)
: Extensions and customizations (in lua) of the awesome window manager.

[emacs.d](https://github.com/winny-/emacs.d)
: Emacs configuration.  Written in literal-programming style.
    Like a good emacs user, a lot of code tailored for my specific needs and
    use-cases (check out `site-*` directories for even more code I wrote or forked).

[dotfiles](https://gitlab.com/winny/dotfiles)
: dotfiles with a tool to manage them using GNU Stow.  Includes
    some useful, original mpv lua scripts too!


### Infrastructure {#infrastructure}

I believe in ensuring I don't duplicate my own work multiple times.  This is
important because computers are pretty messy and easy to break.

[nixos-configs](https://gitlab.com/winny/nixos-configs)
: My desktop configuration.  May adopt it for server use in future.

[ircbox](https://gitlab.com/winny/ircbox)
: My irc setup.  Used primarily via the glowing-bear web application
    &amp; weechat-android app.  Ansible.

[nightflyer](https://github.com/winny-/nightflyer)
: My hobby retrocomputing Thinkpad x31.  Yes it even runs ansible!

[seedbox](https://gitlab.com/winny/seedbox)
: VM to host open source .iso torrents.  Ansible.

[gitlab-runner-playbook](https://gitlab.com/winny/gitlab-runner-playbook)
: Ansible playbook to deploy a gitlab runner on
    Alpine linux.  This takes over the whole system as gitlab runners should not
    be trusted (they run whatever code you push).


### Feature Complete {#feature-complete}

Stuff I consider complete - no more features.  Also stuff that hasn't been
updated recently, however, I depend on it.

[mumble-ping](https://pkgs.racket-lang.org/package/mumble-ping)
: Racket utility library to check mumble server status

[runomatic](https://github.com/winny-/runomatic)
: Racket-based API Client &amp; Bot for runogame.com

[wifi-toggle](https://github.com/winny-/wifi-toggle)
: Web microservice to toggle a Mikrotik router's wifi.  Useful
    for disconnecting from this shared consciousness we call the internet.

[rpaste](https://github.com/winny-/rpaste)
: Pastebin in Racket

[imgurgame](https://github.com/winny-/imgurgame)
: Python flask webapp to show random imgur images by changing
    case of a string.  NSFW because imgur is NSFW.

[ssh-hack](https://github.com/winny-/ssh-hack)
: ssh wrapper to simplify playing NetHack on sites like hardfought.org

[serveip](https://github.com/winny-/serveip)
: Microservice to tell client their public facing IP.  Like
    ipchicken.  Racket.

[etqw-ping-racket](https://github.com/winny-/etqw-ping-racket)
: Library/tool to query Enemey Territory: Quake Wars
    servers in Racket.


### Zprava {#zprava}

Undergrad university capstone project.  Chat app with Android app.  Backend is
django + django-rest-framework.  I was the lead on the backend team.

-   [API spec](https://gitlab.com/zprava/random_files/-/blob/master/documentation/API.md)
-   [Script to test the API implementation with the API.md specification file](https://gitlab.com/zprava/random_files/-/blob/master/documentation/doctest/test.rkt) (Racket)
-   [Backend service](https://gitlab.com/zprava/zprava-api-service)


### Inactive {#inactive}

Codebases in various states of disrepair or irrelevance.  Anything I don't
depend on or isn't in a usable state.

[wine-prefix](https://github.com/winny-/wine-prefix)
: A wine prefix manager written in Racket

[mcstat](https://github.com/winny-/mcstat)
: PHP library to get Minecraft server status

[chdl](https://github.com/winny-/chdl)
: pyton asyncio image downloader in python.  Sadly aiohttp broke
    their backwards compat and I opted to write a new version in Racket.

[abl-mode](https://github.com/winny-/abl-mode)
: OpenEdge Advanced Business Language major mode for emacs.
    Forked from [here](https://github.com/nathanielknight/abl-mode).

[minecraft-ping](https://github.com/winny-/minecraft-ping)
: Racket library to query minecraft servers.

[sc2bank](https://github.com/winny-/sc2bank)
: Python PyQt4 application to modify/sign starcraft II arcade XML documents.

[sirsi](https://github.com/winny-/sirsi) and [sirsi2](https://github.com/winny-/sirsi2)
: Python tools to check when library books become overdue
    / auto renew books if possible.  This was to avoid pesky library fines due
    to not liking their hard to use website.  Did I Mention Sirsi Dynex isn't
    user friendly?

[secret-manager](https://github.com/winny-/secret-manager)
: Simple (probably insecure) python password manager

[s2png-yotsuba-edition](https://github.com/winny-/s2png-yotsuba-edition)
: fork of a tool to embed content into images.  Is
    not a stenographic tool.  C.

[minimal-blog-viewer](https://github.com/winny-/minimal-blog-viewer)
: View Blogger blogs without client side javascript
    (e.g. in lynx).  Written in nodejs.

[minecraft-nbt](https://github.com/winny-/minecraft-nbt)
: Partial implementation of a NBT file parser in Racket.

[lslfragments](https://bitbucket.org/winny-/lslfragments/)
: LindenScript Language scripts for Second Life.  This
    language is a real piece of work.  Don't believe me, read the code :)

More bitbucket repos...?
: I would list them, however, bitbucket had the
    wonderful idea of wiping mercurial repositories instead of archiving them
    until the user deletes them.


### Infrastructure {#infrastructure}

[winny-gentoo-ops](https://gitlab.com/winny-gentoo-ops)
: My old Gentoo workstation configuration (such as `/etc/portage`)

[winny-overlay](https://github.com/winny-/winny-overlay)
: My old Gentoo overlay for Gentoo (portage) packages

[ports](https://github.com/winny-/ports)
: FreeBSD ports I maintained for awhile


### Hermes {#hermes}

For a little while I was the maintainer of the Heremes Pandora App for Mac.
Then I quit using Macs.  It wasn't for me.  Apple doesn't seem to listen to
power users like they used to.

[HermesRemote](https://github.com/winny-/HermesRemote)
: Control the Hermes application over the network.
    Play/pause/skip/control volume.  php+applescript backend with jquery / bootstrap frontend.

[HermesSkypeControl](https://github.com/winny-/HermesSkypeControl)
: Python script to suspend Hermes playback during Skype calls.

[sparkle-util](https://github.com/winny-/sparkle-util)
: scripts to automate an old way of signing applications on
    Mac, prior to notarization.

[Hermes](https://github.com/HermesApp/Hermes)
: The app itself (I no longer maintain this for a long time)


## Open source contributions {#open-source-contributions}

Goal of this document: demonstrate a history of contributing to
projects af various languages and technologies.

I make a lot of small contributions to various open source
projects. My goal is to improve my own "quality of life in computing"
with one fix at a time. As a rule of thumb, I try to use software that
I can fix much like I would try to fix any electronic or furniture I
possess.

**I do not claim ownership or any recognition beyond making important
fixes to various projects.**

Note: This is an exceprted list. It also presently does not include
most of my own projects or tickets that brought about improvements.

---


## FreeBSD (2012-2018) {#freebsd--2012-2018}

I used to be loosely involved with the FreeBSD project as a ports
maintainer. Later on I used this experience at a student job to
write ports for a scientific computing cluster.

<https://github.com/winny-/ports>

[FreeBSD Bugzilla Search for my tickets](https://bugs.freebsd.org/bugzilla/buglist.cgi?email1=winston&email2=Winston%2520Smith&emailassigned_to1=1&emailassigned_to2=1&emailcc1=1&emailcc2=1&emaillongdesc1=1&emaillongdesc2=1&emailreporter1=1&emailreporter2=1&emailtype1=substring&emailtype2=notsubstring&query_format=advanced)

(The one about lack order reversal is not posted by me.)

<https://github.com/outpaddling/freebsd-ports-wip/commits?author=winny->-


## VLC for iOS (2014) {#vlc-for-ios--2014}

Add option to embolden subtitles for easier reading on low contrast
videos.

<https://code.videolan.org/videolan/vlc-ios/commit/0544e2d40f57c516578da39f1717448d7d68556a>


## Hermes (2014-2015) {#hermes--2014-2015}

Hermes is a Pandora client for MacOS. I maintained it for a couple
years, then gave up my hobby projects within the Apple
Ecosystem. This project gave me new appreciation for the amount of
hard work every software maintainer must put in to achieve even the
most basic software quality standards.

<https://github.com/HermesApp/Hermes/commits?author=winny->


### Projects related to Hermes {#projects-related-to-hermes}

-   [HermesRemote](https://github.com/winny-/HermesRemote) - an experimental web UI for controlling Hermes.
-   [HermesSkypeControl](https://github.com/winny-/HermesSkypeControl) - Play/pause Hermes when you receive a call in Skype.


## Gentoo packaging (2011, 2018-2020) {#gentoo-packaging--2011-2018-2020}

Many years ago I made an overlay with [some interesting ebuilds](https://bitbucket.org/winny-/srsbuilds/wiki/Home).

Presently I maintain an overlay of ebuilds [on GitHub](https://github.com/winny-/winny-overlay). It is also on
the list of unofficial overlays that `layman` uses.

I still use Gentoo for various reasons; if something breaks (which
it doesn't), it is my own fault.


## Arch Linux packaging (2017) {#arch-linux-packaging--2017}

I moved on from Arch Linux because of various reasons amounting to a
system that was hard to manage software across deployments, subpar
package writing tools (e.g. not any meaningful QA checks), and a
community that was not pleasant to participate in.

Never the less, I did maintain a few packages in AUR.

-   [libtcod-151](https://aur.archlinux.org/packages/libtcod-151/) (work around Arch's lack of slotting/parallel
    installation support)
-   [crimson](https://aur.archlinux.org/packages/crimson/) (Classic open source game: Crimson Fields)
-   [slashem](https://aur.archlinux.org/packages/slashem/) (nethack variant)
-   [marlowe](https://aur.archlinux.org/packages/marlowe/) (Shakespeare programming language transpiler)


## Textual (2013-2014) {#textual--2013-2014}

Textual is the best IRC client for MacOS. I don't use MacOS anymore,
but I did enjoy improving it. Fixes include UX enhancements, adding
extension AppleScripts, and reporting a bunch of issues that others
fixed. Unfortunately the Textual issue tracker on GitHub was shut
down some time ago. I recall finding a bug with a PING sent from the
server would be replied by a double-PONG, causing certain IRC
daemons to disconnect Textual clients, thereby rendering Textual not
very usable with certain IRC networks.

<https://github.com/Codeux-Software/Textual/commits?author=winny->

I also wrote a little [DeviantArt Thumbnail plugin](https://github.com/winny-/Textual-DAThumbnailPlugin) in Swift for Textual.


## applescript-json (2014) {#applescript-json--2014}

Yes, a JSON encoder in AppleScript. It is madness.

<https://github.com/mgax/applescript-json/commits?author=winny->


## pjson (2014-2015) {#pjson--2014-2015}

Python based json pretty printer with color. Various fixes.

<https://github.com/igorgue/pjson/commits?author=winny->


## glacier-cli (2014) {#glacier-cli--2014}

A simple CLI to access Amazon Web Services Glacier - a nearline
storage service intended for storage that is not intended for online
access.

<https://github.com/carlossg/glacier-cli/commits?author=winny->


## Homebrew (2014) {#homebrew--2014}

A couple package bumps in the de-facto Mac OS X package manager.

<https://github.com/Homebrew/legacy-homebrew/commits?author=winny->


## mcstatus (2015) {#mcstatus--2015}

That Python package written by dinnerbone (a minecraft dev) to ping
minecraft servers. I fixed up the packaging to Pythonista
expectations.

<https://github.com/Dinnerbone/mcstatus/commits?author=winny->


## xbanish (2015) {#xbanish--2015}

Tool to hide mouse. Resolved some packaging quirks.

<https://github.com/jcs/xbanish/commits?author=winny->


## cyberpunk-theme.el (2018-2019) {#cyberpunk-theme-dot-el--2018-2019}

Best theme for emacs. UX improvements.

<https://github.com/jcs/xbanish/commits?author=winny->


## yossarian-bot (2015-2019) {#yossarian-bot--2015-2019}

IRC bot. Mostly adding plugins and dockerizing.

<https://github.com/woodruffw/yossarian-bot/commits?author=winny->


## racket-irc (2015-2018) {#racket-irc--2015-2018}

IRC client library for Racket. Added TLS support.

<https://github.com/woodruffw/yossarian-bot/commits?author=winny->


## Nimdok (2014-2015) {#nimdok--2014-2015}

IRC bot. Tidy up and add some plugins.

<https://github.com/woodruffw/yossarian-bot/commits?author=winny->


## pycodestyle (2014) {#pycodestyle--2014}

Popular python code linter. Formerly called pep8.

<https://github.com/PyCQA/pycodestyle/commits?author=winny->


## mibot (2017) {#mibot--2017}

IRC Bot. Minor fix.

<https://github.com/Nyubis/mibot/commits?author=winny->


## Qutebrowser (2018) {#qutebrowser--2018}

Good keyboard-first web browser. Documentation tweaks.

<https://github.com/qutebrowser/qutebrowser/commits?author=winny->


## slop (2018) {#slop--2018}

Select rectangles, windows, or screens for screenshots and other
tasks.

Document some things, fix a segfault.

<https://github.com/naelstrof/slop/commits?author=winny->


## cava (2018) {#cava--2018}

Text based music visualizer.

Document some keyboard shortcuts.

<https://github.com/karlstav/cava/commits?author=winny->


## emacs-dashboard (2018) {#emacs-dashboard--2018}

Landing page for your emacs.

Document some things. Check back for some QoL fixes.

<https://github.com/emacs-dashboard/emacs-dashboard/commits?author=winny->


## cargo-ebuild (2018) {#cargo-ebuild--2018}

Tool to generate .ebuild files (for packaging software on gentoo)
from Cargo crates (used for the Rust programming language).

Fix a link.

<https://github.com/cardoe/cargo-ebuild/commits?author=winny->


## org-static-blog (2019) {#org-static-blog--2019}

Static website generator.

Fix to RSS feed generation. Add way to force complete regeneration
of the website.

<https://github.com/bastibe/org-static-blog/commits?author=winny->


## typed-racket (2019) {#typed-racket--2019}

Typed variant of Racket.

Add some more types to untyped racket bits.

<https://github.com/racket/typed-racket/commits?author=winny->


## the racket website (2019) {#the-racket-website--2019}

Every site needs a favicon.

<https://github.com/racket/racket-lang-org/commits?author=winny->


## racket (2018-2019) {#racket--2018-2019}

Documentation tweaks. Fix/report issue with GCC/GNU Make detection
in 7.4.

<https://github.com/racket/racket/commits?author=winny->


## Retroarch (2020) {#retroarch--2020}

[Improve some documentation](https://github.com/libretro/docs/commits?author=winny-).


## Alpine (2020) {#alpine--2020}

[Contributing multiple improvements](https://github.com/alpinelinux/aports/commits?author=winny-) to the Alpine Ports project used to build
the packages for Alpine Linux.

P.S. Alpine is pretty nice :)


## [mdadm](https://en.wikipedia.org/wiki/Mdadm) (2020) {#mdadm--2020}

Just [a tiny patch](https://git.kernel.org/pub/scm/utils/mdadm/mdadm.git/commit/?id=5e592e1ed809b94670872b7a4629317fc1c8a5c1) to fix the documentation.
