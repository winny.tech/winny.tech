+++
title = "Writings"
date = 2022-06-24T16:53:00-05:00
draft = false
+++

## Blog {#blog}

See my blog at [`blog.winny.tech`](https://blog.winny.tech/).


## Writing a Worm {#writing-a-worm}

During my computer science undergrad at University of Wisconsin Milwaukee, I
had the opportunity to participate in a cybersecurity lab.

We were tasked with a project towards the end of the class and I authored a
simple computer worm.  Much like [panchan](https://www.linode.com/blog/security/linode-security-digest-panchan-malware-lelastic-vulnerability/), it works by using SSH for lateral
movement across hosts.  Unlike panchan, it was never released into the wild and
is not super robust.

-   [Slide deck](</ox-hugo/Winston Weinert - Presentation - Writing a Worm.pdf>)
-   [Final report](</ox-hugo/Winston Weinert - Report - Writing a Worm.pdf>)
