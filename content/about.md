+++
title = "About"
author = ["Winston Weinert (winny)"]
date = 2021-03-31T19:07:00-05:00
draft = false
cover = "looking-for.jpg"
+++

Hi there!  My name is Winston Weinert --- you may call me Winny.  I am a
Linux and Coffee aficionado.  I'm interested in learning about technology as it
intersects with society and the living beings within our society.

Values include: deliberate actions, mindfulness, empathy, and listening (and
not agreeing to) to everyone.


## Let's eat some food together!! {#let-s-eat-some-food-together}

Yum!

I have a fairly limited diet.  _Such is life._  If you want to cook or cater for
me, please review [this page]({{< relref "food" >}}).  Thank you for your kindness!


## What am I doing now? {#what-am-i-doing-now}

See my ["now"]({{< relref "now" >}}) page.


## Fun stuff {#fun-stuff}

I like to play [roguelikes]({{< relref "roguelikes" >}}).


## Donate {#donate}

Please consider donating to me or to other individuals or organizations in the
open source ecosystem.  [Donate here]({{< relref "donate" >}}).
