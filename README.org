#+startup: indent

* ox-hugo powered homepage

The code is located on [[https://gitlab.com/winny.tech/winny.tech][GitLab official hosting]]. This website and
project files are licensed under [[file:LICENSE.txt][CC-BY-SA-4.0]].

* Signatures

** Creating

#+begin_src bash
  gpg --detach --armor --sign YOURFILE
#+end_src

** Verifying

#+begin_src bash
  gpg --verify YOURFILE
#+end_src
