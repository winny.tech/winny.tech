THEME_DIR=themes/winny2-theme

dev: prepare
	hugo server --buildFuture --buildDrafts --disableFastRender --watch

prepare: node_modules

node_modules: $(THEME_DIR)/node_modules
	ln -sf $< $@

$(THEME_DIR)/node_modules: $(THEME_DIR)/package.json
	cd $(THEME_DIR) && npm i

clean:
	rm -f node_modules
	rm -rf $(THEME_DIR)/node_modules

.PHONY: dev prepare clean
